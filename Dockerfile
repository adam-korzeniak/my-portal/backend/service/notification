FROM eclipse-temurin:21
MAINTAINER adamkorzeniak.com
COPY target/notification-*.jar notification.jar
EXPOSE 8443
ENTRYPOINT ["java","-jar","/notification.jar"]