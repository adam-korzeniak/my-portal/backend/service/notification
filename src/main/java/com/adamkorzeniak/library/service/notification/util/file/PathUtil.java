package com.adamkorzeniak.library.service.notification.util.file;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.File;

@NoArgsConstructor(access = AccessLevel.NONE)
public class PathUtil {

    public static String getAbsolutePath() {
        return getAbsolutePath(".");
    }

    public static String getAbsolutePath(String path) {
        File file = new File(path);
        return file.getAbsolutePath();
    }
}
