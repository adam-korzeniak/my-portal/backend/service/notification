package com.adamkorzeniak.library.service.notification.domain.mail.dto;

import com.adamkorzeniak.library.service.notification.util.email.Email;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class MailDto {

    private final String subject;
    private final String message;

    protected MailDto(MailDto mailDto) {
        this(mailDto.subject, mailDto.message);
    }

    public static MailDto of(String subject, String message) {
        return new MailDto(subject, message);
    }

    public static AddressedMailDto to(Email email, String subject, String message) {
        return new AddressedMailDto(email, subject, message);
    }

    public static AddressedMailDto to(Email email, MailDto mailDto) {
        return new AddressedMailDto(email, mailDto);
    }

    public String subject() {
        return subject;
    }

    public String message() {
        return message;
    }
}
