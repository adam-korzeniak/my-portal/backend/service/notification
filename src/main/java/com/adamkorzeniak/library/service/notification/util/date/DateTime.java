package com.adamkorzeniak.library.service.notification.util.date;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.time.OffsetDateTime;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DateTime {
    private final OffsetDateTime value;

    public static DateTime now() {
        return new DateTime(OffsetDateTime.now());
    }

    public boolean isAfter(DateTime dateTime) {
        return this.value.isAfter(dateTime.value);
    }

    public int getHour() {
        return value.getHour();
    }

    public DateTime plusMinutes(long minutes) {
        return new DateTime(value.plusMinutes(minutes));
    }
}
