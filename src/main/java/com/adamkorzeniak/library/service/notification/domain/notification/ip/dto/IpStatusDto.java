package com.adamkorzeniak.library.service.notification.domain.notification.ip.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Set;

@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class IpStatusDto {

    private final Status status;
    private final Set<String> ipAddresses;

    public static IpStatusDto ok(Set<String> ipAddresses) {
        return new IpStatusDto(Status.OK, ipAddresses);
    }

    public static IpStatusDto changed(Set<String> ipAddresses) {
        return new IpStatusDto(Status.CHANGED, ipAddresses);
    }

    public static IpStatusDto failure() {
        return new IpStatusDto(Status.PROCESS_FAILED, Set.of());
    }

    public static IpStatusDto ignored() {
        return ignored(Set.of());
    }

    public static IpStatusDto ignored(Set<String> ipAddresses) {
        return new IpStatusDto(Status.CHANGED, ipAddresses);
    }

    public boolean hasChanged() {
        return status == Status.CHANGED;
    }

    public boolean hasFailed() {
        return status == Status.PROCESS_FAILED;
    }

    public Status status() {
        return status;
    }

    public boolean hasAddresses() {
        return !ipAddresses.isEmpty();
    }

    public Set<String> addresses() {
        return ipAddresses;
    }
    public enum Status {
        OK, CHANGED, PROCESS_FAILED, PROCESS_IGNORED
    };
}
