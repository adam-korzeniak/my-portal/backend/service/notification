package com.adamkorzeniak.library.service.notification.util.email;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Email {
    private final String value;

    public static Email of(String value) {
        return new Email(value);
    }

    public String value() {
        return value;
    }

}
