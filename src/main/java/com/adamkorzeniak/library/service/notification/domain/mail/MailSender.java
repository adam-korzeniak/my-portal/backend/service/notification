package com.adamkorzeniak.library.service.notification.domain.mail;

import com.adamkorzeniak.library.service.notification.domain.mail.dto.AddressedMailDto;
import com.adamkorzeniak.library.service.notification.domain.mail.dto.MailDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class MailSender {

    private final MailConfiguration mailConfiguration;
    private final JavaMailSender emailSender;

    public void sendMessage(MailDto mail) {
        sendMessage(MailDto.to(mailConfiguration.fromEmail(), mail));
    }

    public void sendMessage(AddressedMailDto mail) {
        log.info("Sending message to {}, subject: {}", mail.to().value(), mail.subject());
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailConfiguration.fromEmail().value());
        message.setTo(mail.to().value());
        message.setSubject(mail.subject());
        message.setText(mail.message());
        emailSender.send(message);
    }
}
