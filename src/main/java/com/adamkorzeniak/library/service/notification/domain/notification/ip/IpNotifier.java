package com.adamkorzeniak.library.service.notification.domain.notification.ip;

import com.adamkorzeniak.library.service.notification.domain.notification.ip.dto.IpStatusDto;
import com.adamkorzeniak.library.service.notification.domain.mail.MailSender;
import com.adamkorzeniak.library.service.notification.domain.mail.dto.MailDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class IpNotifier {

    private static final int PORTAL_PORT = 3000;
    private static final int BACKEND_PORT = 8443;

    private final IpVerifier ipVerifier;
    private final MailSender mailSender;

    public void verifyIpDetails() {
        IpStatusDto ipStatusDto = ipVerifier.checkIpStatus();
        if (ipStatusDto.hasChanged() || ipStatusDto.hasFailed()) {
            log.info("Starting ip notification process due to status: {}", ipStatusDto);
            notifyAboutIpDetails(ipStatusDto);
        }
    }

    public void notifyAboutIpDetails() {
        IpStatusDto ipStatusDto = ipVerifier.checkIpStatus();
        log.info("Starting ip notification process");
        notifyAboutIpDetails(ipStatusDto);
    }

    private void notifyAboutIpDetails(IpStatusDto ipStatusDto) {
        String subject = buildSubject(ipStatusDto);
        String message = buildMessage(ipStatusDto);
        mailSender.sendMessage(MailDto.of(subject, message));
    }

    private String buildSubject(IpStatusDto ipStatusDto) {
        return switch (ipStatusDto.status()) {
            case OK -> "Ip address working correctly";
            case CHANGED -> "CHANGE: Ip address changed";
            case PROCESS_FAILED, PROCESS_IGNORED -> "ERROR: IP verification failure";
        };
    }

    private String buildMessage(IpStatusDto ipStatusDto) {
        StringBuilder message = new StringBuilder(buildSubject(ipStatusDto));
        if (ipStatusDto.hasAddresses()) {
            appendAddressDetails(ipStatusDto, message);
        }
        return message.toString();
    }

    private static void appendAddressDetails(IpStatusDto ipStatusDto, StringBuilder message) {
        String addresses = String.join("\n", ipStatusDto.addresses());
        String portalUrls = ipStatusDto.addresses().stream()
                .map(IpNotifier::buildPortalUrl)
                .collect(Collectors.joining("\n"));
        String addressesMessage =
                """
                        Addresses:
                        %s
                                            
                        Portal Urls
                        %s
                        """;
        message.append(String.format(addressesMessage, addresses, portalUrls));
    }

    private static String buildPortalUrl(String ip) {
        return String.format("https://[%s]:%s/config?uri=https://[%s]:%s",
                ip, PORTAL_PORT, ip, BACKEND_PORT);
    }
}
