package com.adamkorzeniak.library.service.notification.domain.notification.ip;

import com.adamkorzeniak.library.service.notification.util.date.DateTime;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class ProcessResult {
    private boolean success;
    private DateTime executionTime;


    static ProcessResult success() {
        return new ProcessResult(true, DateTime.now());
    }

    static ProcessResult failure() {
        return new ProcessResult(false, DateTime.now());
    }
}