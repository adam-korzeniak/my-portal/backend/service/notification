package com.adamkorzeniak.library.service.notification.domain.notification.ip;

import com.adamkorzeniak.library.service.notification.domain.notification.ip.dto.IpStatusDto;
import com.adamkorzeniak.library.service.notification.util.date.DateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Set;

@Slf4j
@Component
@RequiredArgsConstructor
class IpVerifier {

    private final IpProvider ipProvider;
    private final IpConfig config;
    private final IpDao ipDao;

    synchronized IpStatusDto checkIpStatus() {
        log.info("Checking ip addresses status");
        Set<String> currentAddresses = ipProvider.getHostIpv6Addresses();
        if (currentAddresses.isEmpty()) {
            log.warn("No IPv6 addresses found");
            return ofFailure();
        }
        boolean addressesChanged = hasAddressesChanged(currentAddresses);
        ipDao.updateAddresses(currentAddresses);
        return addressesChanged
                ? IpStatusDto.changed(currentAddresses)
                : IpStatusDto.ok(currentAddresses);
    }

    private IpStatusDto ofFailure() {
        IpStatusDto result = ipDao.hasLastProcessSucceeded()
                ? IpStatusDto.failure()
                : ofRepeatedFailure();
        if (result.hasFailed()) {
            ipDao.markProcessFailed();
        }
        return result;
    }

    //TODO: This logic is related if we should notify user,
    // This should be placed in notification related logic
    private IpStatusDto ofRepeatedFailure() {
        DateTime now = DateTime.now();
        int currentHour = now.getHour();
        DateTime nextFailureNotification = ipDao.getLastProcessExecutionTime().plusMinutes(config.repeatedErrorSnoozeInMinutes());

        boolean afterEarlyMorning = currentHour >= config.repeatedErrorStartHour();
        boolean beforeLateNight = currentHour < config.repeatedErrorEndHour();
        boolean snoozeErrorTimeElapsed = now.isAfter(nextFailureNotification);
        boolean shouldTriggerFailure = afterEarlyMorning && beforeLateNight && snoozeErrorTimeElapsed;

        log.info("Repeated failure verification result: {}, not to early: {}, not to late: {}, snooze time elapsed: {}",
                shouldTriggerFailure, afterEarlyMorning, beforeLateNight, shouldTriggerFailure);

        return shouldTriggerFailure
                ? IpStatusDto.failure()
                : IpStatusDto.ignored();
    }

    boolean hasAddressesChanged(Set<String> currentAddresses) {
        Set<String> lastAddresses = ipDao.getAddresses();
        boolean notChanged = lastAddresses.size() == currentAddresses.size()
                && lastAddresses.containsAll(currentAddresses);
        log.info("Addresses changed: {}, previous: {}, current: {}", !notChanged, lastAddresses, currentAddresses);
        return !notChanged;
    }
}
