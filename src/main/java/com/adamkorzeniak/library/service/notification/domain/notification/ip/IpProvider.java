package com.adamkorzeniak.library.service.notification.domain.notification.ip;

import com.adamkorzeniak.library.service.notification.util.file.FileReader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
class IpProvider {

    private final IpConfig config;

    Set<String> getHostIpv6Addresses() {
        return getFileContent().stream()
                .filter(Objects::nonNull)
                .filter(line -> !line.isBlank())
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private List<String> getFileContent() {
        try {
            List<String> lines = FileReader.readAllLines(config.ipv6AddressesFileLocation());
            log.info("Loaded IPv6 file with {} lines, Content: {}", lines.size(), lines);
            return lines;
        } catch (IOException e) {
            log.error("Failed to read IPv6 file", e);
            return new ArrayList<>();
        }
    }
}
