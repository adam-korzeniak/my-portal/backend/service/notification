package com.adamkorzeniak.library.service.notification.inbound;

import com.adamkorzeniak.library.service.notification.domain.notification.ip.IpNotifier;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class IpNotificationScheduledTasks {

    private final IpNotifier ipNotifier;

    @Scheduled(fixedRate = 2 * 60 * 1000)
    public void verifyIpDetails() {
        log.info("Starting scheduled ip verification process");
        ipNotifier.verifyIpDetails();
        log.info("Finished scheduled ip verification process");
    }

    @Scheduled(cron = "0 0 6 * * *")
    public void notifyAboutIpDetails() {
        log.info("Starting scheduled ip notification process");
        ipNotifier.notifyAboutIpDetails();
        log.info("Finished scheduled ip notification process");
    }

}