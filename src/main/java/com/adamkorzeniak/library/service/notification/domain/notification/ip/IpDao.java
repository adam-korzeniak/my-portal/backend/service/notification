package com.adamkorzeniak.library.service.notification.domain.notification.ip;

import com.adamkorzeniak.library.service.notification.util.date.DateTime;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
class IpDao {
    private Set<String> lastAddresses = Set.of();
    private ProcessResult lastProcessResult = ProcessResult.success();

    synchronized void updateAddresses(Set<String> currentAddresses) {
        this.lastAddresses = currentAddresses;
    }

    synchronized public Set<String> getAddresses() {
        return this.lastAddresses;
    }

    synchronized boolean hasLastProcessSucceeded() {
        return lastProcessResult.isSuccess();
    }

    synchronized void markProcessFailed() {
        this.lastProcessResult = ProcessResult.failure();
    }

    synchronized DateTime getLastProcessExecutionTime() {
        return lastProcessResult.getExecutionTime();
    }
}
