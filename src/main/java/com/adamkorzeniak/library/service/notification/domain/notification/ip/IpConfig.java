package com.adamkorzeniak.library.service.notification.domain.notification.ip;

import org.springframework.stereotype.Component;

@Component
class IpConfig {
    //TODO: Move to yml
    private static final String IPV6_ADDRESSES_FILE_LOCATION = "Resources/data/ip_addresses.txt";
    private static final int REPEATED_ERROR_IGNORE_FOR_MINUTES = 6 * 60;
    private static final int REPEATED_ERROR_START_HOUR = 6;
    private static final int REPEATED_ERROR_END_HOUR = 22;

    String ipv6AddressesFileLocation() {
        return IPV6_ADDRESSES_FILE_LOCATION;
    }

    int repeatedErrorSnoozeInMinutes() {
        return REPEATED_ERROR_IGNORE_FOR_MINUTES;
    }

    int repeatedErrorStartHour() {
        return REPEATED_ERROR_START_HOUR;
    }

    int repeatedErrorEndHour() {
        return REPEATED_ERROR_END_HOUR;
    }
}
