package com.adamkorzeniak.library.service.notification.domain.mail.dto;

import com.adamkorzeniak.library.service.notification.util.email.Email;

public class AddressedMailDto extends MailDto {

    private final Email to;

    AddressedMailDto(Email to, String subject, String message) {
        super(subject, message);
        this.to = to;
    }

    public AddressedMailDto(Email to, MailDto mailDto) {
        super(mailDto);
        this.to = to;
    }

    public Email to() {
        return to;
    }
}
